import pandas as pd, zipfile, glob, time
from datetime import datetime, timedelta
import pickle, os
import logging

LOG_FORMAT = "%(asctime)s - %(message)s"
logging.basicConfig(filename = "C:\\Users\\steba\\FOREX Unpacker\\log\\CSVmerger.log",
                    level = logging.INFO,
                    format = LOG_FORMAT,
                    filemode = "w")
logger = logging.getLogger()

def time_converter(title, miliseconds):
    d = timedelta(seconds=int(miliseconds) / 1000)
    date = (datetime(year=int(title[:4]), month=int(title[4:6]),
      day=int(title[6:8]))) + d
    print(date)
    return date

def pair_merger(pairs):
    """Merges all sub-.cvs for each pair in the given list into one dataframe"""
    logger.info("Initializing FOREX pair merging")

    #print(f"{(datetime.utcnow().strftime('%H:%M:%S'))} - Initializing FOREX pair merging")

    pair_count = 0
    forex_start_time = time.time()

    # Check that input is a list of pairs
    if type(pairs)!=list:
        list_of_pairs = []
        list_of_pairs.append(pairs)
    else:
        list_of_pairs = pairs

    for pair in list_of_pairs:
        subcount = 0
        pair_start_time = time.time()
        for filename in glob.iglob(f"data\{pair}-unpacked\*.csv"):
            df = pd.read_csv(filename, names=[
             'time', 'askclose', 'askhigh',
             'asklow', 'askopen', 'volume',
             'bidclose', 'bidhigh', 'bidlow',
             'bidopen', 'volume2', 'close',
             'high', 'low', 'open', 'volume3'],
              dtype={'time':float,
             'askclose':float,  'askhigh':float,  'asklow':float,
             'askopen':float,  'volume':float,  'bidclose':float,
             'bidhigh':float,  'bidlow':float,  'bidopen':float,
             'volume2':float,  'close':float,  'high':float,
             'low':float,  'open':float,  'volume3':float})

            df['time'] = df['time'].apply(lambda x: time_converter(filename[21:], x))
            df[pair] = 0
            df[pair] = (df.askclose + df.bidclose) / 2

            df.rename(columns={'askclose':f"{pair}_askclose",  'bidclose':f"{pair}_bidclose"}, inplace=True)

            df.index = [df.time]
            df = df[[f"{pair}_askclose", f"{pair}_bidclose", f"{pair}"]]

            df.to_pickle(f"{pair}-{subcount}")
            subcount += 1

        logger.info(f"{pair} .csv conversion time {(time.time() - pair_start_time)}")
        logger.info(f"Processing saved dfs")

        main_df = pd.DataFrame()

        for filename in glob.iglob(f"{pair}-*"):
            df1 = pd.read_pickle(filename)
            if len(main_df) == 0:
                main_df = df1
            else:
                print(filename)
                main_df = main_df.append(df1)
                del(df1)

        main_df.to_pickle(f"{pair}")
        logger.info(f"{pair} - Processing complete - main_df pickled")

        for filename in glob.iglob(f"{pair}-*"):
            os.remove(filename)
        logger.info("Removed old files succesfully")

        pair_count += 1

    print(f'''{(datetime.utcnow().strftime('%H:%M:%S'))} - Completed {pair_count} pairs''')
    print(f'''Total duration: {(time.time() - forex_start_time)}''')
    logger.info(f"Completed {pair_count} pairs")
    logger.info(f"Total duration: {(time.time() - forex_start_time)}")