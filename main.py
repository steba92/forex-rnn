import pandas as pd
import zipfile
import glob
import time
from datetime import datetime, timedelta
import pickle
import os
from CSVmerger import time_converter, pair_merger
import multiprocessing as mp

print(f"{datetime.utcnow().strftime('%H:%M:%S')} - Initializing .csv processor")

all_pairs = []
for pair in glob.iglob("data/*-unpacked"):
    all_pairs.append(pair[5:11])

start_time = time.time()

if __name__ == '__main__':
    p = mp.Pool(processes=2)
    p.map(pair_merger, all_pairs)

print(f"{datetime.utcnow().strftime('%H:%M:%S')} - Completed {len(all_pairs)} pairs in {time.time()-start_time} seconds ")