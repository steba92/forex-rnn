"Add multi-processing functionality???"

def unZipAll():
    "Unzips all .zip files in data/* folders"
    start = time.time()
    print("Unzipping files")

    files = 0
    bad = 0
    for pair in glob.iglob("data/*"):
        print(f"{datetime.utcnow().strftime('%H:%M:%S')} - Unzipping {pair}")
        for filename in glob.iglob(f"{pair}/*"):
            try:
                with zipfile.ZipFile(filename, "r") as zip_ref:
                    zip_ref.extractall(f"data/{pair[5:]}-unpacked")
                    files += 1
            except:
                print("Bad ZipFile: ", filename)
                bad += 1
    print(f"Unpacked {files} files with {bad} errors.")
    print(f"Time to complete: {time.time()-start}")